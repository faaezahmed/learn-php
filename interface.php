<?php 
	interface Animal {
		public function speak();
	}

	class dog implements Animal
	{
		public function speak() {
			echo 'test';
		}
		public static function woof() {
			echo 'woof';
		}
	}

	$ab = new dog();
	dog::woof();
	
	var_dump($ab->speak());
?>